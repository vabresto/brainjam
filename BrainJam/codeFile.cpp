#include "codeFile.h"

#include <stdio.h>	// file rename
#include <iostream>
#include <fstream>
#include <cstring>	//memset
#include <tinyfiledialogs/tinyfiledialogs.h>
#include <stack>

#include <imgui-util/imgui-util.h>

const std::string ide::codeFile::blankFileName = "-blank-";

ide::codeFile::codeFile()
	: m_fileName(blankFileName)
	, m_isSaved(false)
	, m_wasReset(false)
	, m_code(nullptr)
	, m_colourized(nullptr)
{
	m_code = new char [m_codeSize];
	std::memset(m_code, '\0', m_codeSize);
	m_colourized = new char [m_codeSize];
	std::memset(m_colourized, '\0', m_codeSize);
}

ide::codeFile::~codeFile()
{
	delete[] m_code;
	delete[] m_colourized;
}

const std::string& ide::codeFile::GetFileName() const
{
	return m_fileName;
}

bool ide::codeFile::IsSaved() const
{
	return m_isSaved;
}

void ide::codeFile::Save()
{
	if(m_fileName == blankFileName)
	{
		SaveAs();
		return;
	}

	SaveEx();
}

void ide::codeFile::SaveAs()
{
	char const * lFilterPatterns[] = { "*.b" };
	char const * lTheSaveFileName = tinyfd_saveFileDialog("Save", "code.b", 1, lFilterPatterns, nullptr);

	if(!lTheSaveFileName)
	{
		std::cerr << "No save file name!" << std::endl;
		return;
	}

	m_fileName = lTheSaveFileName;

	SaveEx();
}

void ide::codeFile::SaveEx()
{
	std::string tempExt = ".tmp";
	std::string code = m_code;


	std::ofstream file(m_fileName + tempExt);

	file << code;

	if(!file)
	{
		std::cerr << "Failed to write to file! Uh oh..." << std::endl;
		return;
	}

	if(rename((m_fileName + tempExt).c_str(), m_fileName.c_str()) != 0)
	{
		std::cerr << "Failed to rename file! Your data should still be saved in " << m_fileName + tempExt << std::endl;
		return;
	}

	m_isSaved = true;
}

bool ide::codeFile::New()
{
	int userWants = 1;
	// Ask user if they want to save if they made edits
	if(!m_isSaved && !(strcmp(m_code, "") == 0))
	{
		// 0 = no	(I want to save)
		// 1 = yes 	(I don't want to save)
		userWants = tinyfd_messageBox(
		                "Unsaved work!",
		                "You still have unsaved work. Do you want to continue without saving?",
		                "yesno",
		                "question",
		                0);
	}

	// Return so user can save
	if(userWants == 0)
	{
		return false;
	}

	// Reset to new file
	m_isSaved = false;
	m_fileName = blankFileName;
	m_wasReset = true;

	std::memset(m_code, '\0', m_codeSize);
	std::memset(m_colourized, '\0', m_codeSize);
	return true;
}

void ide::codeFile::Update()
{
	m_isSaved = false;
}

void ide::codeFile::Load()
{
	char const * lFilterPatterns[] = { "*.b" };
	char const * lTheLoadFileName = tinyfd_openFileDialog("Load File", "", 1, lFilterPatterns, nullptr, 0);

	if(!lTheLoadFileName)
	{
		std::cerr << "No load file name!" << std::endl;
		return;
	}


	std::fstream file(lTheLoadFileName);
	
	std::memset(m_code, '\0', m_codeSize);
	std::memset(m_colourized, '\0', m_codeSize);

	std::string contents((std::istreambuf_iterator<char>(file)),
	                     std::istreambuf_iterator<char>());
	std::memcpy(m_code, contents.c_str(), contents.size());

	RecalculateColours();

	m_isSaved = true;
	m_fileName = lTheLoadFileName;
}

void ide::codeFile::RecalculateColours()
{
	// Keep a stack with open braces
	std::stack<std::pair<size_t, bool>> openBraces;

	// There might be a way to do this without iterating through everything ...
	for(size_t idx = 0; idx <= m_codeSize; ++idx)
	{
		const char c = m_code[idx];

		if(c == '\0' || c == '\t' || c == '\n')
		{
			continue;
		}
		else if(c == '+')
		{
			m_colourized[idx] = static_cast<char>(imgui_util::SyntaxColours::Plus);
		}
		else if(c == '-')
		{
			m_colourized[idx] = static_cast<char>(imgui_util::SyntaxColours::Minus);
		}
		else if(c == '<')
		{
			m_colourized[idx] = static_cast<char>(imgui_util::SyntaxColours::LeftShift);
		}
		else if(c == '>')
		{
			m_colourized[idx] = static_cast<char>(imgui_util::SyntaxColours::RightShift);
		}
		else if(c == ',')
		{
			m_colourized[idx] = static_cast<char>(imgui_util::SyntaxColours::Read);
		}
		else if(c == '.')
		{
			m_colourized[idx] = static_cast<char>(imgui_util::SyntaxColours::Write);
		}
		else if(c == '[')
		{
			if(m_cursorPos != idx)
			{
				m_colourized[idx] = static_cast<char>(imgui_util::SyntaxColours::OpenBrace);
			}
			else
			{
				//Cursor is here
				m_colourized[idx] = static_cast<char>(imgui_util::SyntaxColours::HighlightedBrace);
			}
			openBraces.push(std::make_pair(idx, true));
		}
		else if(c == ']')
		{
			// True means open brace, false means close brace
			if(!openBraces.empty() && openBraces.top().second == true)
			{
				if(m_cursorPos != idx
				        && m_colourized[openBraces.top().first]
				        != static_cast<char>(imgui_util::SyntaxColours::HighlightedBrace))
				{
					m_colourized[idx] = static_cast<char>(imgui_util::SyntaxColours::CloseBrace);
				}
				else
				{
					//Cursor is here
					m_colourized[idx] = static_cast<char>(imgui_util::SyntaxColours::HighlightedBrace);

					// Adjust colour of other brace
					m_colourized[openBraces.top().first] = static_cast<char>(imgui_util::SyntaxColours::HighlightedBrace);
				}

				openBraces.pop();
			}
			else
			{
				openBraces.push(std::make_pair(idx, false));
			}
		}
		else
		{
			m_colourized[idx] = static_cast<char>(imgui_util::SyntaxColours::Regular);
		}
	}

	while(!openBraces.empty())
	{
		if(m_colourized[openBraces.top().first] == static_cast<char>(imgui_util::SyntaxColours::HighlightedBrace)
		        || openBraces.top().first == m_cursorPos)
		{
			m_colourized[openBraces.top().first] = static_cast<char>(imgui_util::SyntaxColours::MisMatchedHighlightedBrace);
		}
		else
		{
			m_colourized[openBraces.top().first] = static_cast<char>(imgui_util::SyntaxColours::MisMatchedBrace);
		}
		openBraces.pop();
	}
}
