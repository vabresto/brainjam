#ifndef INSTRUCTIONLABEL_H
#define INSTRUCTIONLABEL_H

#include "instruction.h"

namespace compiler
{
	class instructionLabel : public instruction
	{
		public:
			instructionLabel(std::string labelName);
			~instructionLabel();

		public:
			virtual const char* GetAsOpCode(blackboard& board) const;
			virtual size_t GetRequiredBytes(blackboard& board) const;
			virtual void MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const;

		private:
			std::string m_labelName;
	};
}

#endif