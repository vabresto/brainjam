#include "instructionValDecrement.h"
#include "compiler/utility/util.h"

namespace compiler
{
	instructionValDecrement::instructionValDecrement(const unsigned int amount)
		: instruction(instruction::instructionType::ValDecrement)
		, m_amount(amount)
	{
		m_opCode = m_opCodeSub;
		util::WriteBytesToString(m_opCode, static_cast<unsigned char>(amount));
	}

	instructionValDecrement::~instructionValDecrement()
	{
	}

	const char* instructionValDecrement::GetAsOpCode(blackboard& board) const
	{
		return m_opCode.c_str();
	}
	size_t instructionValDecrement::GetRequiredBytes(blackboard& board) const
	{
		return m_opCode.size();
	}
}

void compiler::instructionValDecrement::MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const
{
	breaks.insert(init + m_opCode.size());
}
