#include "instructionWrite.h"

#include "compiler/utility/util.h"

namespace compiler
{
	instructionWrite::instructionWrite(blackboard& board)
		: instruction(instruction::instructionType::Write)
		, isInWriteMode(false)
	{
		//isInWriteMode = board.GetBuildingFlag(building::Flags::EAX_WriteMode);

		m_opCode = m_opCodeEdx;
		
		util::WriteBytesToString(m_opCode, static_cast<uint32_t>(1));
		
		m_opCode += m_opCodeEcx;
		m_opCode += m_opCodeEbx;
		if(!isInWriteMode)
		{
			// If not already in write mode, set to write mode
			m_opCode += m_opCodeEax;
			board.SetBuildingFlag(building::Flags::EAX_WriteMode, true);
		}
		m_opCode += m_opCodeExecute;
	}
	
	instructionWrite::instructionWrite(blackboard& board, size_t numToPrint)
		: instruction(instruction::instructionType::Write)
		, isInWriteMode(false)
	{
		//isInWriteMode = board.GetBuildingFlag(building::Flags::EAX_WriteMode);

		m_opCode = m_opCodeEdx;
		
		util::WriteBytesToString(m_opCode, static_cast<uint32_t>(numToPrint));
		
		m_opCode += m_opCodeEcx;
		m_opCode += m_opCodeEbx;
		if(!isInWriteMode)
		{
			// If not already in write mode, set to write mode
			m_opCode += m_opCodeEax;
			board.SetBuildingFlag(building::Flags::EAX_WriteMode, true);
		}
		m_opCode += m_opCodeExecute;
	}

	instructionWrite::~instructionWrite()
	{
	}

	const char* instructionWrite::GetAsOpCode(blackboard& board) const
	{
		return m_opCode.c_str();
	}
	size_t instructionWrite::GetRequiredBytes(blackboard& board) const
	{
		return m_opCode.size();
	}
}

void compiler::instructionWrite::MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const
{
	// This is really ugly + unreadable :(

	const size_t edxSize = 5;	// The total size should be 5, but it has 'fill-in-the-blanks'
	
	breaks.insert(init + edxSize);
	breaks.insert(init + edxSize + m_opCodeEcx.size());
	breaks.insert(init + edxSize + m_opCodeEcx.size() + m_opCodeEbx.size());
	if(!isInWriteMode)
	{
		breaks.insert(init + edxSize + m_opCodeEcx.size() + m_opCodeEbx.size() + m_opCodeEax.size());
		breaks.insert(init + edxSize + m_opCodeEcx.size() + m_opCodeEbx.size() + m_opCodeEax.size() + m_opCodeExecute.size());
	}
	else
	{
		breaks.insert(init + edxSize + m_opCodeEcx.size() + m_opCodeEbx.size() + m_opCodeExecute.size());
	}
}
