#include "instructionValIncrement.h"

#include "instruction.h"
#include "compiler/utility/util.h"

#include <iostream>

namespace compiler
{
	instructionValIncrement::instructionValIncrement(const unsigned int amount)
		: instruction(instruction::instructionType::ValIncrement)
		, m_amount(amount)
	{
		m_opCode = m_opCodeAdd;
		util::WriteBytesToString(m_opCode, static_cast<unsigned char>(amount));
	}

	instructionValIncrement::~instructionValIncrement()
	{
	}

	const char* instructionValIncrement::GetAsOpCode(blackboard& board) const
	{
		return m_opCode.c_str();
	}
	size_t instructionValIncrement::GetRequiredBytes(blackboard& board) const
	{
		return m_opCode.size();
	}
}

void compiler::instructionValIncrement::MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const
{
	breaks.insert(init + m_opCode.size());
}
