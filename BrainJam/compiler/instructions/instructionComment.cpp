#include "instructionComment.h"

namespace compiler
{
	instructionComment::instructionComment(std::string comment)
		: instruction(instruction::instructionType::Comment)
		, m_comment(comment)
	{
	}

	instructionComment::~instructionComment()
	{
	}

	const char* instructionComment::GetAsOpCode(blackboard& board) const
	{
		return "";
	}
	size_t instructionComment::GetRequiredBytes(blackboard& board) const
	{
		return 0;
	}
}

void compiler::instructionComment::MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const
{
}
