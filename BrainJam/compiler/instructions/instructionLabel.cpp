#include "instructionLabel.h"

namespace compiler
{
	instructionLabel::instructionLabel(std::string labelName)
		: instruction(instruction::instructionType::Label)
		, m_labelName(labelName)
	{
	}

	instructionLabel::~instructionLabel()
	{
	}

	const char* instructionLabel::GetAsOpCode(blackboard& board) const
	{
		return m_labelName.c_str();
	}
	size_t instructionLabel::GetRequiredBytes(blackboard& board) const
	{
		return 0;
	}
}

void compiler::instructionLabel::MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const
{
}
