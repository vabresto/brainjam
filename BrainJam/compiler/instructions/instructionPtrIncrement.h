#ifndef INSTRUCTIONPTRINCREMENT_H
#define INSTRUCTIONPTRINCREMENT_H

#include "instruction.h"

namespace compiler
{
	class instructionPtrIncrement : public instruction
	{
		public:
			explicit instructionPtrIncrement(const unsigned int amount = 1);
			~instructionPtrIncrement();

		public:
			virtual const char* GetAsOpCode(blackboard& board) const;
			virtual size_t GetRequiredBytes(blackboard& board) const;
			virtual void MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const;

		private:
			const unsigned int m_amount;

		private:
			const std::string m_opCodeAdd =
			{
				'\x81', '\xC7', //'\x01', '\x00', '\x00', '\x00',		// add edi, 0x01
			};
	};
}

#endif // INSTRUCTIONPTRINCREMENT_H
