#ifndef INSTRUCTIONJUMPTARGET_H
#define INSTRUCTIONJUMPTARGET_H

#include "instruction.h"

namespace compiler
{
	class instructionJumpTarget : public instruction
	{
		public:
			static const std::string labelPrefix;

		public:
			explicit instructionJumpTarget(std::string name);
			~instructionJumpTarget();

		public:
			virtual const char* GetAsOpCode(blackboard& board) const;
			virtual size_t GetRequiredBytes(blackboard& board) const;
			virtual void MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const;
			virtual bool Link(const size_t baseAddr, const size_t curAddr, const labelStore& labels) override;

		private:
			std::string m_name;

		private:
			const std::string jump =
			{
				'\xE9', // '\xFF', '\xFF', '\xFF', '\xFF', '\xFF',	// jmp relative
			};

	};
}

#endif // INSTRUCTIONJUMPTARGET_H
