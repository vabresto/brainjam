#ifndef INSTRUCTIONVALDECREMENT_H
#define INSTRUCTIONVALDECREMENT_H

#include "instruction.h"

namespace compiler
{
	class instructionValDecrement : public instruction
	{
		public:
			explicit instructionValDecrement(const unsigned int amount = 1);
			~instructionValDecrement();

		public:
			virtual const char* GetAsOpCode(blackboard& board) const;
			virtual size_t GetRequiredBytes(blackboard& board) const;
			virtual void MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const;

		private:
			const unsigned int m_amount;

		private:
			const std::string m_opCodeSub =
			{
				'\x80', '\x2F', //'\x01', '\x00', '\x00', '\x00',		// sub DWORD PTR [edi], 0x01
			};
	};
}

#endif // INSTRUCTIONVALDECREMENT_H
