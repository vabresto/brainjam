#include "instructionPtrDecrement.h"

#include "compiler/utility/util.h"

namespace compiler
{
	instructionPtrDecrement::instructionPtrDecrement(const unsigned int amount)
		: instruction(instruction::instructionType::PtrDecrement)
		, m_amount(amount)
	{
		m_opCode = m_opCodeSub;
		util::WriteBytesToString(m_opCode, static_cast<uint32_t>(amount));
	}

	instructionPtrDecrement::~instructionPtrDecrement()
	{
	}

	const char* instructionPtrDecrement::GetAsOpCode(blackboard& board) const
	{
		return m_opCode.c_str();
	}
	size_t instructionPtrDecrement::GetRequiredBytes(blackboard& board) const
	{
		return m_opCode.size();
	}
}

void compiler::instructionPtrDecrement::MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const
{
	breaks.insert(init + m_opCode.size());
}
