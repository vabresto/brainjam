#include "instruction.h"

namespace compiler
{
	instruction::instruction(instructionType type)
		: m_type(type)
		, m_linked(true)
	{
	}

	instruction::instruction(instructionType type, bool linked)
		: m_type(type)
		, m_linked(linked)
	{
	}

	instruction::~instruction()
	{
	}
}

bool compiler::instruction::Link(const size_t baseAddr, const size_t curAddr, const labelStore& labels)
{
	return m_linked;
}
