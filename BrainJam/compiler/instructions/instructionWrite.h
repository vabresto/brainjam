#ifndef INSTRUCTIONWRITE_H
#define INSTRUCTIONWRITE_H

#include "instruction.h"
#include "../utility/blackboard.h"

namespace compiler
{
	class instructionWrite : public instruction
	{
		public:
			explicit instructionWrite(blackboard& board);
			instructionWrite(blackboard& board, size_t numToPrint);
			~instructionWrite();

		public:
			virtual const char* GetAsOpCode(blackboard& board) const;
			virtual size_t GetRequiredBytes(blackboard& board) const;
			virtual void MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const;

		private:
			bool isInWriteMode;

		private:
			const std::string m_opCodeEax =
			{
				'\xB8', '\x04', '\x00', '\x00', '\x00',		// mov eax, 4
			};
			const std::string m_opCodeEbx =
			{
				'\xBB', '\x00', '\x00', '\x00', '\x00',		// mov ebx, 0
			};
			const std::string m_opCodeEcx =
			{
				'\x89', '\xF9',								// mov ecx, edi
			};
			const std::string m_opCodeEdx =
			{
				'\xBA', //'\x01', '\x00', '\x00', '\x00',	// mov edx, 1
			};
			const std::string m_opCodeExecute =
			{
				'\xCD', '\x80',								// int 80
			};
	};
}

#endif // INSTRUCTIONWRITE_H
