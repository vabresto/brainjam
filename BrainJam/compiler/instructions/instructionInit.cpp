#include "instructionInit.h"

#include <iostream>
#include <iomanip>

#include "compiler/utility/util.h"

namespace compiler
{
	const std::string instructionInit::initLabelName = "__END_OF_PROG";

	instructionInit::instructionInit()
		: instruction(instruction::instructionType::Init, false)
	{
		m_opCode = m_opCodeEdi;
		m_opCode += { '\xFF', '\xFF', '\xFF', '\xFF', };	// Filler bytes - will get replaced during link
	}

	instructionInit::~instructionInit()
	{
	}

	const char* instructionInit::GetAsOpCode(blackboard& board) const
	{
		return m_opCode.c_str();
	}

	size_t instructionInit::GetRequiredBytes(blackboard& board) const
	{
		return m_opCode.size();
	}

	void instructionInit::MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const
	{
		breaks.insert(init + m_opCode.size());
	}
}

bool compiler::instructionInit::Link(const size_t baseAddr, const size_t curAddr, const labelStore& labels)
{
	size_t address;

	if(labels.GetLabel(initLabelName, address))
	{
		address += baseAddr;
		address += 1;
		
		//Make 32 bit
		uint32_t addr32 = static_cast<uint32_t>(address);

		// Reset opcode
		m_opCode = m_opCodeEdi;

		util::WriteBytesToString(m_opCode, addr32);

		return true;
	}

	return false;
}
