#ifndef INSTRUCTIONREAD_H
#define INSTRUCTIONREAD_H

#include "instruction.h"
#include "../utility/blackboard.h"

namespace compiler
{
	class instructionRead : public instruction
	{
		public:
			explicit instructionRead(blackboard& board);
			~instructionRead();

		public:
			virtual const char* GetAsOpCode(blackboard& board) const;
			virtual size_t GetRequiredBytes(blackboard& board) const;
			virtual void MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const;

		private:
			bool isInReadMode;

		private:
			const std::string m_opCodeEax =
			{
				'\xB8', '\x03', '\x00', '\x00', '\x00',		// mov eax, 3
			};
			const std::string m_opCodeEbx =
			{
				'\xBB', '\x00', '\x00', '\x00', '\x00',		// mov ebx, 0
			};
			const std::string m_opCodeEcx =
			{
				'\x89', '\xF9',								// mov ecx, edi
			};
			const std::string m_opCodeEdx =
			{
				'\xBA', '\x01', '\x00', '\x00', '\x00',		// mov edx, 1
			};
			const std::string m_opCodeExecute =
			{
				'\xCD', '\x80',								// int 80
			};
	};
}

#endif // INSTRUCTIONREAD_H
