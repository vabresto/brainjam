#ifndef INSTRUCTIONJUMPSTART_H
#define INSTRUCTIONJUMPSTART_H

#include "instruction.h"

namespace compiler
{
	class instructionJumpStart : public instruction
	{
		public:
			static const std::string labelPrefix;

		public:
			explicit instructionJumpStart(std::string name);
			~instructionJumpStart();

		public:
			virtual const char* GetAsOpCode(blackboard& board) const;
			virtual size_t GetRequiredBytes(blackboard& board) const;
			virtual void MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const;
			virtual bool Link(const size_t baseAddr, const size_t curAddr, const labelStore& labels) override;

		private:
			std::string m_name;

		private:
			const std::string loadEax =
			{
				'\x8A', '\x07',											// mov eax, [edi]
				//'\x8B', '\x0F',
			};
			const std::string cmpEax =
			{
				'\x3D', '\x00', '\x00', '\x00', '\x00',					// cmp eax, 0
				//'\x83', '\xF9', '\x00',
			};
			const std::string jmpNEZ =
			{
				'\x0F', '\x85', '\x05', '\x00', '\x00', '\x00',			// jmp if not zero
			};

			const std::string loadEcx =
			{
				'\x8B', '\x0F'											// mov contents of [edi] to ecx
				//'\xB9', '\x01', '\x00', '\x00', '\x00',				// set ecx to value
			};
			const std::string checkEcx =
			{
				'\xE3', '\x05',											// jmp 5 bytes forward if ecx == 0
			};

			const std::string jumpEnd =
			{
				'\xE9', // '\xFF', '\xFF', '\xFF', '\xFF',				// jmp to end
			};
	};
}

#endif // INSTRUCTIONJUMPSTART_H
