#ifndef INSTRUCTIONVALINCREMENT_H
#define INSTRUCTIONVALINCREMENT_H

#include "instruction.h"

namespace compiler
{
	class instructionValIncrement : public instruction
	{
		public:
			explicit instructionValIncrement(const unsigned int amount = 1);
			~instructionValIncrement();

		public:
			virtual const char* GetAsOpCode(blackboard& board) const;
			virtual size_t GetRequiredBytes(blackboard& board) const;
			virtual void MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const;

		private:
			const unsigned int m_amount;

		private:
			const std::string m_opCodeAdd =
			{
				'\x80', '\x07', //'\x01', '\x00', '\x00', '\x00',		// add DWORD PTR [edi], 0x01	// add [mem], imm
			};
	};
}

#endif // INSTRUCTIONVALINCREMENT_H
