#ifndef INSTRUCTIONPTRDECREMENT_H
#define INSTRUCTIONPTRDECREMENT_H

#include "instruction.h"

namespace compiler
{
	class instructionPtrDecrement : public instruction
	{
		public:
			explicit instructionPtrDecrement(const unsigned int amount = 1);
			~instructionPtrDecrement();

		public:
			virtual const char* GetAsOpCode(blackboard& board) const;
			virtual size_t GetRequiredBytes(blackboard& board) const;
			virtual void MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const;

		private:
			const unsigned int m_amount;

		private:
			const std::string m_opCodeSub =
			{
				'\x81', '\xEF', //'\x01', '\x00', '\x00', '\x00',		// sub edi, 0x01
			};
	};
}

#endif // INSTRUCTIONPTRDECREMENT_H
