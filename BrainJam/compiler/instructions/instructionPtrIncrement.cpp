#include "instructionPtrIncrement.h"

#include "compiler/utility/util.h"

namespace compiler
{
	instructionPtrIncrement::instructionPtrIncrement(const unsigned int amount)
		: instruction(instruction::instructionType::PtrIncrement)
		, m_amount(amount)
	{
		m_opCode = m_opCodeAdd;
		util::WriteBytesToString(m_opCode, static_cast<uint32_t>(amount));
	}

	instructionPtrIncrement::~instructionPtrIncrement()
	{
	}

	const char* instructionPtrIncrement::GetAsOpCode(blackboard& board) const
	{
		return m_opCode.c_str();
	}
	size_t instructionPtrIncrement::GetRequiredBytes(blackboard& board) const
	{
		return m_opCode.size();
	}
}

void compiler::instructionPtrIncrement::MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const
{
	breaks.insert(init + m_opCode.size());
}
