#ifndef INSTRUCTIONINIT_H
#define INSTRUCTIONINIT_H

#include "instruction.h"


namespace compiler
{
	class instructionInit : public instruction
	{
	public:
		static const std::string initLabelName;
		
		public:
			instructionInit();
			~instructionInit();

		public:
			virtual const char* GetAsOpCode(blackboard& board) const;
			virtual size_t GetRequiredBytes(blackboard& board) const;
			virtual void MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const;
			virtual bool Link(const size_t baseAddr, const size_t curAddr, const labelStore& labels) override;

		private:
			

		private:
			const std::string m_opCodeEdi = 
			{
				'\xBF', //'\xFF', '\xFF', '\xFF', '\xFF', 	// mov edi, FF FF FF FF
			};
	};
}

#endif