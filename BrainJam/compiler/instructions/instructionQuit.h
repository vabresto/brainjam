#ifndef INSTRUCTIONQUIT_H
#define INSTRUCTIONQUIT_H

#include "instruction.h"


namespace compiler
{
	class instructionQuit : public instruction
	{
		public:
			instructionQuit();
			~instructionQuit();

		public:
			virtual const char* GetAsOpCode(blackboard& board) const;
			virtual size_t GetRequiredBytes(blackboard& board) const;
			virtual void MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const;

		private:

		private:
			const std::string m_opCodeEax =
			{
				'\xB8', '\x01', '\x00', '\x00', '\x00',   	// mov eax, 1
			};
			const std::string m_opCodeExecute =
			{
				'\xCD', '\x80',                            	// int 0x80
			};
	};
}

#endif