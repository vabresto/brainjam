#include "instructionQuit.h"

namespace compiler
{
	instructionQuit::instructionQuit()
		: instruction(instruction::instructionType::Quit)
	{
		m_opCode = m_opCodeEax;
		m_opCode += m_opCodeExecute;
	}

	instructionQuit::~instructionQuit()
	{
	}

	const char* instructionQuit::GetAsOpCode(blackboard& board) const
	{
		return m_opCode.c_str();
	}

	size_t instructionQuit::GetRequiredBytes(blackboard& board) const
	{
		return m_opCode.size();
	}

	void instructionQuit::MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const
	{
		breaks.insert(init + m_opCodeEax.size());
		breaks.insert(init + m_opCodeEax.size() + m_opCodeExecute.size());
	}
}
