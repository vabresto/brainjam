#ifndef INSTRUCTIONCOMMENT_H
#define INSTRUCTIONCOMMENT_H

#include "instruction.h"

namespace compiler
{
	class instructionComment : public instruction
	{
		public:
			instructionComment(std::string comment);
			~instructionComment();

		public:
			virtual const char* GetAsOpCode(blackboard& board) const;
			virtual size_t GetRequiredBytes(blackboard& board) const;
			virtual void MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const;

		private:
			std::string m_comment;
	};
}

#endif // INSTRUCTIONCOMMENT_H
