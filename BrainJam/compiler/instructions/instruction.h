#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include <string>
#include <set>

#include "compiler/utility/blackboard.h"
#include "compiler/utility/label.h"

namespace compiler
{
	class instruction
	{
		public:
			enum class instructionType : char
			{
			    INVALID = -1,
			    Comment,			// Do nothing

			    ValIncrement,		// Increment current memory location
			    ValDecrement,		// Decrement current memory location
			    PtrIncrement,		// ???
			    PtrDecrement,		// ???
			    Read,				// Read a word from stdin
			    Write,				// ???
			    JumpStart,			// ???
			    JumpTarget,			// ???

			    //Used behind the scenes only
			    Init,				// Setup environment
			    Quit,				// Return
				Label,
			    // Do not create any enum entries beyond this point
			    Count
			};

		protected:
			instruction(instructionType type);
			instruction(instructionType type, bool linked);
		public:
			~instruction();

		public:
			instructionType GetType() const
			{
				return m_type;
			}
			bool IsLinked() const
			{
				return m_linked;
			}
			virtual const char* GetAsOpCode(blackboard& board) const = 0;
			virtual size_t GetRequiredBytes(blackboard& board) const = 0;
			virtual void MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const = 0;
			virtual bool Link(const size_t baseAddr, const size_t curAddr, const labelStore& labels);

		private:
			instructionType m_type;

		protected:
			bool m_linked;
			std::string m_opCode;
	};
}

#endif
