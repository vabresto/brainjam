#include "instructionJumpStart.h"

#include <string>
#include <iostream>

#include "compiler/utility/util.h"
#include "compiler/instructions/instructionJumpTarget.h"

namespace compiler
{
	const std::string instructionJumpStart::labelPrefix = "Opn@";

	instructionJumpStart::instructionJumpStart(std::string name)
		: instruction(instruction::instructionType::JumpStart)
		, m_name(name)
	{
		m_opCode = loadEax;
		m_opCode += cmpEax;
		m_opCode += jmpNEZ;

		//m_opCode = loadEcx;
		//m_opCode += checkEcx;


		m_opCode += jumpEnd;
		m_opCode += { '\xFF', '\xFF', '\xFF', '\xFF',  };
	}

	instructionJumpStart::~instructionJumpStart()
	{
	}

	const char* instructionJumpStart::GetAsOpCode(blackboard& board) const
	{
		return m_opCode.c_str();
	}
	size_t instructionJumpStart::GetRequiredBytes(blackboard& board) const
	{
		return m_opCode.size();
	}
}

void compiler::instructionJumpStart::MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const
{
	breaks.insert(init + loadEax.size());
	breaks.insert(init + loadEax.size() + cmpEax.size());
	breaks.insert(init + loadEax.size() + cmpEax.size() + jmpNEZ.size());
	
	
//	breaks.insert(init + loadEcx.size());
//	breaks.insert(init + loadEcx.size() + checkEcx.size());
	
	
	breaks.insert(init + m_opCode.size());
}

bool compiler::instructionJumpStart::Link(const size_t baseAddr, const size_t curAddr, const labelStore& labels)
{
	size_t address;

	std::string lookingforlabel = instructionJumpTarget::labelPrefix + m_name;

	std::cout << "Looking for label " << lookingforlabel << std::endl;

	if(labels.GetLabel(lookingforlabel, address))
	{
		//Make 32 bit
		uint32_t addr32 = static_cast<uint32_t>(address);

		std::cout << "[Address]: " << address << std::endl;
		std::cout << "[Base]: " << baseAddr << std::endl;
		std::cout << "[Curr]: " << curAddr << std::endl;

		// We have to account for our size as well
		//	since we start counting the offset from the end
		//	of the jump instruction
		addr32 -= (curAddr + m_opCode.size());
		std::cout << "[OFFSET]: " << addr32 << std::endl;

		// Reset opcode
		m_opCode = loadEax;
		m_opCode += cmpEax;
		m_opCode += jmpNEZ;

		//m_opCode = loadEcx;
		//m_opCode += checkEcx;

		m_opCode += jumpEnd;
		util::WriteBytesToString(m_opCode, addr32);

		return true;
	}

	return false;
}
