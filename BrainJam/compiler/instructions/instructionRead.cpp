#include "instructionRead.h"

namespace compiler
{
	instructionRead::instructionRead(blackboard& board)
		: instruction(instruction::instructionType::Read)
		, isInReadMode(false)
	{
		//isInReadMode = board.GetBuildingFlag(building::Flags::EAX_ReadMode);

		m_opCode = m_opCodeEdx;
		m_opCode += m_opCodeEcx;
		m_opCode += m_opCodeEbx;
		if(!isInReadMode)
		{
			// If not already in write mode, set to write mode
			m_opCode += m_opCodeEax;
			board.SetBuildingFlag(building::Flags::EAX_ReadMode, true);
		}
		m_opCode += m_opCodeExecute;
	}

	instructionRead::~instructionRead()
	{
	}

	const char* instructionRead::GetAsOpCode(blackboard& board) const
	{
		return m_opCode.c_str();
	}
	size_t instructionRead::GetRequiredBytes(blackboard& board) const
	{
		return m_opCode.size();
	}
}

void compiler::instructionRead::MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const
{
	// This is really ugly + unreadable :(

	breaks.insert(init + m_opCodeEdx.size());
	breaks.insert(init + m_opCodeEdx.size() + m_opCodeEcx.size());
	breaks.insert(init + m_opCodeEdx.size() + m_opCodeEcx.size() + m_opCodeEbx.size());
	if(!isInReadMode)
	{
		breaks.insert(init + m_opCodeEdx.size() + m_opCodeEcx.size() + m_opCodeEbx.size() + m_opCodeEax.size());
		breaks.insert(init + m_opCodeEdx.size() + m_opCodeEcx.size() + m_opCodeEbx.size() + m_opCodeEax.size() + m_opCodeExecute.size());

	}
	else
	{
		breaks.insert(init + m_opCodeEdx.size() + m_opCodeEcx.size() + m_opCodeEbx.size() + m_opCodeExecute.size());
	}
}
