#include "instructionJumpTarget.h"

#include <string>
#include <iostream>

#include "compiler/utility/util.h"

namespace compiler
{
	const std::string instructionJumpTarget::labelPrefix = "TgtFor";
	
	instructionJumpTarget::instructionJumpTarget(std::string name)
		: instruction(instruction::instructionType::JumpTarget)
		, m_name(name)
	{
		m_opCode = jump;
		m_opCode += { '\xFF', '\xFF', '\xFF', '\xFF',  };
	}

	instructionJumpTarget::~instructionJumpTarget()
	{
	}

	const char* instructionJumpTarget::GetAsOpCode(blackboard& board) const
	{
		return m_opCode.c_str();
	}
	size_t instructionJumpTarget::GetRequiredBytes(blackboard& board) const
	{
		return m_opCode.size();
	}
}

void compiler::instructionJumpTarget::MarkDebugLineBreaks(std::set<size_t>& breaks, const size_t init) const
{
	breaks.insert(init + m_opCode.size());
}

bool compiler::instructionJumpTarget::Link(const size_t baseAddr, const size_t curAddr, const labelStore& labels)
{
size_t address;

	std::string lookingforlabel = m_name;

	std::cout << "Looking for label " << lookingforlabel << std::endl;

	if(labels.GetLabel(lookingforlabel, address))
	{
		//Make SIGNED 32 bit
		int32_t addr32 = static_cast<int32_t>(address);
		
		//addr32 = -48;		// -5 causes infinite loop (but only prints once), -7 causes one run
							// with first byte '\x00'
							// and code: >[+.]
		
		std::cout << "[Address]: " << address << std::endl;
		std::cout << "[Base]: " << baseAddr << std::endl;
		std::cout << "[Curr]: " << curAddr << std::endl;


		addr32 -= curAddr;
		// We start counting at the end of the jump,
		//	so we need to account for and skip ourselves too
		addr32 -= m_opCode.size();	
		
		std::cout << "[OFFSET]: " << addr32 << std::endl;
		
		// Reset opcode
		m_opCode = jump;

		util::WriteBytesToString(m_opCode, addr32);

		return true;
	}

	return false;
}