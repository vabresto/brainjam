#include "builder.h"
#include <compiler/instructions/instructionComment.h>
#include <compiler/instructions/instructionValIncrement.h>
#include <compiler/instructions/instructionValDecrement.h>
#include <compiler/instructions/instructionPtrIncrement.h>
#include <compiler/instructions/instructionPtrDecrement.h>
#include <compiler/instructions/instructionRead.h>
#include <compiler/instructions/instructionWrite.h>
#include <compiler/instructions/instructionJumpStart.h>
#include <compiler/instructions/instructionJumpTarget.h>

#include <compiler/instructions/instructionInit.h>
#include <compiler/instructions/instructionQuit.h>
#include <compiler/instructions/instructionLabel.h>

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <string>

#include <ide/views/console.h> // For logging

namespace
{
	std::string stripNonCode(const std::string& input)
	{
		std::string ret = "";
		for(const auto & letter : input)
		{
			if(letter != '+' && letter != '-'
			        && letter != '>' && letter != '<'
			        && letter != '[' && letter != ']'
			        && letter != '.' && letter != ','
			  )
			{
				continue;
			}

			ret += letter;
		}
		return ret;
	}
}

compiler::builder::builder(const std::string program, size_t memorySize, char fillChar)
	: m_progStartAddress(0x08048000)
	, m_memorySize(memorySize)
	, m_fillChar(fillChar)
{
	tokenize(stripNonCode(program));
}

compiler::builder::~builder()
{
}


void compiler::builder::tokenize(const std::string& code)
{
	if(code.empty())
	{
		return;
	}

	AddInstruction(compiler::instructionInit());

	unsigned int amount = 1;
	for(size_t idx = 0; idx < code.size(); ++idx)
	{
		const char op = code[idx];

		const size_t next = idx + 1;
		if(next != code.size() - 1)
		{
			if(code[next] == op &&
			        (op == '+' || op == '-' || op == '>' || op == '<'))
			{
				++amount;
				continue;
			}
		}

		switch(op)
		{
			case '+':
			{
				AddInstruction(compiler::instructionValIncrement(amount));
				break;
			}
			case '-':
			{
				AddInstruction(compiler::instructionValDecrement(amount));

				break;
			}
			case '>':
			{
				AddInstruction(compiler::instructionPtrIncrement(amount));
				break;
			}
			case '<':
			{
				AddInstruction(compiler::instructionPtrDecrement(amount));
				break;
			}
			case '.':
			{
				AddInstruction(compiler::instructionWrite(m_blackboard));
				break;
			}
			case ',':
			{
				AddInstruction(compiler::instructionRead(m_blackboard));
				break;
			}
			case '[':
			{
				std::string tempOpenLabel = compiler::instructionJumpStart::labelPrefix;
				tempOpenLabel += std::to_string(m_instructions.size());

				// The label goes BEFORE the conditional check
				AddInstruction(compiler::instructionLabel(tempOpenLabel));
				m_openBracesLabels.push_back(tempOpenLabel);

				// Now conditional check
				AddInstruction(compiler::instructionJumpStart(tempOpenLabel));
				break;
			}
			case ']':
			{
				if(m_openBracesLabels.empty())
				{
					std::cerr << "[ERROR]: No matching open brace for closing brace!" << std::endl;
					g_Console.AddLog(ide::Console::LogLevel::Error, "No matching open brace for closing brace!\n");
					m_instructions.clear();
					return;
				}
				std::string tempMatchingLabel = m_openBracesLabels.back();
				m_openBracesLabels.pop_back();

				// The unconditional jump first
				AddInstruction(compiler::instructionJumpTarget(tempMatchingLabel));

				// The label goes AFTER the unconditional instruction
				AddInstruction(compiler::instructionLabel(compiler::instructionJumpTarget::labelPrefix + tempMatchingLabel));
				break;
			}
			default:
			{
				continue;
			}
		}
		amount = 1;
	}

	//Print everything in memory
	//AddInstruction(compiler::instructionInit());
	//AddInstruction(compiler::instructionWrite(m_blackboard, m_memorySize));

	// Return
	AddInstruction(compiler::instructionQuit());

	// Necessary to compile! This must be the last instruction!
	// This keeps track of the writeable memory we have
	AddInstruction(compiler::instructionLabel(compiler::instructionInit::initLabelName));

	if(!m_openBracesLabels.empty())
	{
		std::cerr << "[ERROR]: No matching closing brace(s)!" << std::endl;
		g_Console.AddLog(ide::Console::LogLevel::Error, "No matching closing brace(s)!\n");
		m_instructions.clear();
		return;
	}
}

bool compiler::builder::compile()
{
	// This function + link can be made more efficient

	// Compile first pass
	for(const auto & instruct : m_instructions)
	{
		if(instruct->GetType() == instruction::instructionType::Label)
		{
			SetLabel(instruct->GetAsOpCode(m_blackboard));
			continue;
		}

		const size_t numChars = instruct->GetRequiredBytes(m_blackboard);
		const char* chars = instruct->GetAsOpCode(m_blackboard);

		instruct->MarkDebugLineBreaks(m_debugNewlineIndices, m_compiled.size());

		for(size_t cnt = 0; cnt < numChars; ++ cnt)
		{
			m_compiled.push_back(chars[cnt]);
		}
	}

	//Print
	g_Console.AddLog(ide::Console::LogLevel::Info, "Compiled (%zu) bytes!\n", m_compiled.size());
	std::cout << std::endl << "***Compiled (" << m_compiled.size() << "):***" << std::endl;
	PrintBytes();
	std::cout << std::endl << "***Labels:***" << std::endl;
	m_labels.PrintLabels();

	// Link
	return link();
}

template <typename T>
void compiler::builder::AddInstruction(const T& instruct)
{
	m_instructions.push_back(std::make_unique<T>(instruct));
}

size_t compiler::builder::compiledSize() const
{
	return m_instructions.size();
}

bool compiler::builder::link()
{
	// Link/Resolve labels
	m_compiled.clear();
	for(const auto & instruct : m_instructions)
	{
		if(!instruct->Link(m_progStartAddress, m_compiled.size(), m_labels))
		{
			std::cerr << "[ERROR]: Failed to link!" << std::endl;
			g_Console.AddLog(ide::Console::LogLevel::Error, "Failed to link!\n");
			return false;
		}

		const size_t numChars = instruct->GetRequiredBytes(m_blackboard);
		const char* chars = instruct->GetAsOpCode(m_blackboard);
		for(size_t cnt = 0; cnt < numChars; ++ cnt)
		{
			m_compiled.push_back(chars[cnt]);
		}
	}

	g_Console.AddLog(ide::Console::LogLevel::Info, "Linked (%zu) bytes!\n", m_compiled.size());
	std::cout << std::endl << "***Linked (" << m_compiled.size() << "):***" << std::endl;
	PrintBytes();

	return true;
}

void compiler::builder::SetLabel(const std::string name)
{
	m_labels.SetLabel(name, m_compiled.size());
}

void compiler::builder::PrintBytes() const
{
	std::ios::fmtflags prevCoutFlags(std::cout.flags());

	for(size_t cnt = 0; cnt < m_compiled.size(); ++cnt)
	{
		if(m_debugNewlineIndices.count(cnt))
		{
			std::cout << std::endl;
		}

		std::cout
		        << std::hex << std::setfill('0') << std::setw(2)
		        << static_cast<unsigned int>(static_cast<unsigned char>(m_compiled[cnt])) << " ";
	}
	std::cout << std::endl;

	std::cout.flags(prevCoutFlags);
}

bool compiler::builder::buildExecutable(std::string outName)
{
	using namespace ELFIO;

	// Compile the program
	compile();

	// Write executable to file
	elfio writer;

	// You can't proceed without this function call!
	writer.create(ELFCLASS32, ELFDATA2LSB);

	writer.set_os_abi(ELFOSABI_LINUX);
	writer.set_type(ET_EXEC);
	writer.set_machine(EM_386);

	// Create code section
	section* text_sec = writer.sections.add(".text");
	text_sec->set_type(SHT_PROGBITS);
	text_sec->set_flags(SHF_ALLOC | SHF_EXECINSTR);
	text_sec->set_addr_align(0x10);

	text_sec->set_data(reinterpret_cast<char*>(m_compiled.data()), m_compiled.size());

	// Create a loadable segment
	segment* text_seg = writer.segments.add();
	text_seg->set_type(PT_LOAD);
	text_seg->set_virtual_address(m_progStartAddress);
	text_seg->set_physical_address(m_progStartAddress);
	text_seg->set_flags(PF_X | PF_R);
	text_seg->set_align(0x1000);

	// Add code section into program segment
	text_seg->add_section_index(text_sec->get_index(), text_sec->get_addr_align());

	// Create data section*
	section* data_sec = writer.sections.add(".data");
	data_sec->set_type(SHT_PROGBITS);
	data_sec->set_flags(SHF_ALLOC | SHF_WRITE);
	data_sec->set_addr_align(0x1);


	char data[m_memorySize];
	std::fill_n(data, m_memorySize, '\x00');

	data_sec->set_data(data, sizeof(data));

	// Get address of end of program
	size_t address;
	if(!m_labels.GetLabel(instructionInit::initLabelName, address))
	{
		std::cerr << "Invalid program being compiled! No terminating instruction!" << std::endl;
		g_Console.AddLog(ide::Console::LogLevel::Error, "Invalid program being compiled!\nNo terminating instruction!\n");
		return false;
	}

	// Create a read/write segment
	segment* data_seg = writer.segments.add();
	data_seg->set_type(PT_LOAD);
	data_seg->set_virtual_address(m_progStartAddress + address);
	data_seg->set_physical_address(m_progStartAddress + address);
	data_seg->set_flags(PF_W | PF_R);
	data_seg->set_align(0x1);

	// Add code section into program segment
	data_seg->add_section_index(data_sec->get_index(), data_sec->get_addr_align());

	section* note_sec = writer.sections.add(".note");
	note_sec->set_type(SHT_NOTE);
	note_sec->set_addr_align(1);
	note_section_accessor note_writer(writer, note_sec);
	note_writer.add_note(0x01, "Created by BrainJam", 0, 0);

	// Setup entry point
	writer.set_entry(m_progStartAddress);

	// Create ELF file
	writer.save(outName);
	return true;
}
