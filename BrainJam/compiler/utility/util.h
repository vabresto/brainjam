#ifndef COMPILER_UTIL_H
#define COMPILER_UTIL_H

#include <string>

namespace compiler
{
	namespace util
	{
		template <typename T>
		void WriteBytesToString(std::string& str, T bytes)
		{
			// Read +insert byte-by-byte
			for(size_t i = 0; i < sizeof(bytes); ++i)
			{
				// Convert to unsigned char* because a char is 1 byte in size.
				// That is guaranteed by the standard.
				// Note that is it NOT required to be 8 bits in size.
				unsigned char byte = *((unsigned char *)&bytes + i);
				str += byte;
			}
		}
	}
}

#endif
