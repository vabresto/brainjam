#ifndef BLACKBOARD_H
#define BLACKBOARD_H

#include <map>

namespace compiler
{
	namespace building
	{
		enum class Flags
		{
			EAX_WriteMode,
			EAX_ReadMode,
		};
	}
	
	class blackboard
	{
		public:
			blackboard();
			~blackboard();
			
	public:
			bool GetBuildingFlag(building::Flags flag);
			void SetBuildingFlag(building::Flags flag, bool setting);
			
	private:
			std::map<building::Flags, bool> m_buildingFlags;

	};
}

#endif // BLACKBOARD_H
