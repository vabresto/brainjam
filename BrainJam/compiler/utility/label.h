#ifndef LABEL_H
#define LABEL_H

#include <string>
#include <vector>

namespace compiler
{
	class label
	{
		friend class labelStore;
		private:
			label(std::string name, size_t location)
				: m_name(name)
				, m_byteLocationRel(location)
				, m_hasAddress(true)
			{}

			explicit label(std::string name)
				: m_name(name)
				, m_byteLocationRel(0)
				, m_hasAddress(false)
			{}

		private:
			std::string m_name;
			size_t m_byteLocationRel;	// Location relative to program start address
			bool m_hasAddress;

		public:
			const std::string& GetName() const
			{
				return m_name;
			}
			const size_t GetRelativeLocation() const
			{
				return m_byteLocationRel;
			}
			const bool HasAddress() const
			{
				return m_hasAddress;
			}
			void Set(size_t address)
			{
				m_byteLocationRel = address;
				m_hasAddress = true;
			}
	};
	
	class labelStore
	{
	private:
		std::vector<label> m_labels;
		
	public:
		void SetLabel(const std::string& name, const size_t address);
		void PrintLabels() const;
		bool GetLabel(const std::string& name, size_t& address) const;
	};
}

#endif
