#include "label.h"

#include <stdio.h>
#include <sstream>
#include <iomanip>

void compiler::labelStore::SetLabel(const std::string& name, const size_t address)
{
	for (auto& lab : m_labels)
	{
		if(lab.GetName() == name)
		{
			lab.Set(address);
			return;
		}
	}

	m_labels.push_back(label(name, address));
}

void compiler::labelStore::PrintLabels() const
{
	std::stringstream ss;
	printf("| %-14s| %-19s|\n", "NAME", "ADDRESS");
	printf("|---------------|--------------------|\n");
	for(const auto & lab : m_labels)
	{
		ss.str("INVALID ADDRESS");
		
		if (lab.HasAddress())
		{
			ss.str("");
			ss << std::hex << lab.GetRelativeLocation();
		}
		
		printf("| %-14s| %-19s|\n", lab.GetName().c_str(), ss.str().c_str());
	}
}

bool compiler::labelStore::GetLabel(const std::string& name, size_t& address) const
{
	for (const auto& lab : m_labels)
	{
		if (lab.GetName() == name)
		{
			address = lab.GetRelativeLocation();
			return true;
		}
	}
	return false;
}
