#include "blackboard.h"

namespace compiler
{
	blackboard::blackboard()
	{
	}

	blackboard::~blackboard()
	{
	}
}

bool compiler::blackboard::GetBuildingFlag(building::Flags flag)
{
	const auto& itr = m_buildingFlags.find(flag);
	
	if (itr != m_buildingFlags.end())
	{
		return itr->second;
	}
	else
	{
		// Add it
		SetBuildingFlag(flag, false);
		return false;
	}
}

void compiler::blackboard::SetBuildingFlag(building::Flags flag, bool setting)
{
	m_buildingFlags[flag] = setting;
	
	// Handle propogation
	if (flag == building::Flags::EAX_WriteMode && setting)
	{
		m_buildingFlags[building::Flags::EAX_ReadMode] = false;
	}
	else if (flag == building::Flags::EAX_ReadMode && setting)
	{
		m_buildingFlags[building::Flags::EAX_WriteMode] = false;
	}
}
