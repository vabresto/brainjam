#ifndef BUILDER_H
#define BUILDER_H

#include <vector>
#include <set>
#include <memory>
#include <string>

#include <elfio/elfio.hpp>

#include "compiler/instructions/instruction.h"
#include "compiler/utility/blackboard.h"
#include "compiler/utility/label.h"

namespace compiler
{
	class builder
	{
		public:
			explicit builder(const std::string program, size_t memorySize = 256, char fillChar = '\x5E');
			~builder();

		private:
			template <typename T>
			void AddInstruction(const T& instruct);

		private:
			const size_t m_progStartAddress;
			size_t m_memorySize;
			char m_fillChar;

		private:
			std::vector<std::unique_ptr<compiler::instruction>> m_instructions;
			std::vector<char> m_compiled;
			labelStore m_labels;
			std::set<size_t> m_debugNewlineIndices;
			blackboard m_blackboard;
			std::vector<std::string> m_openBracesLabels;

		public:
			void SetLabel(const std::string name);
			size_t compiledSize() const;
			void PrintBytes() const;

			void tokenize(const std::string& code);
			bool compile();
			bool link();
			bool buildExecutable(std::string outName);
	};
}

#endif // BUILDER_H
