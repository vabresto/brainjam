#ifndef CODEFILE_H
#define CODEFILE_H

#include <string>

namespace ide
{

	class codeFile
	{
	public:
		const static std::string blankFileName;
	
	public:
		codeFile();
		~codeFile();
			
	private:
		std::string m_fileName;
		bool m_isSaved;
			
	public:
		bool m_wasReset;
		char* m_code;
		char* m_colourized;
		const size_t m_codeSize = 1024 * 64;
		size_t m_cursorPos;
			
	public:
		const std::string& GetFileName() const;
		bool IsSaved() const;
			
		void Update();
		void RecalculateColours();
		void Save();
		void SaveAs();
		bool New();
		void Load();
			
	private:
		void SaveEx();
	};

}

#endif // CODEFILE_H
