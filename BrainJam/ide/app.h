#ifndef APP_H
#define APP_H

#include <SFML/Graphics.hpp>

namespace ide 
{
	// Forward declare
	class codeFile;
	
	void runWindow();
	
	bool compile(codeFile& file, std::string fileName = "a.out");
	bool compileAndRun(codeFile& file);
}

#endif