#include "app.h"

#include <iostream>

#include <imgui/imgui.h>
#include <imgui-sfml/imgui-SFML.h>
#include <imgui-util/imgui-util.h>

#include <titlebar.h>
#include <codeFile.h>
#include <views/homepage.h>
#include <views/console.h>
#include <future>
#include <thread>

void ide::runWindow()
{
	sf::RenderWindow window(sf::VideoMode(800, 600), "BrainJam IDE");

	// Imgui init stuff
	ImGui::SFML::Init(window, false);
	sf::Clock deltaClock;
	imgui_util::SetupImGuiStyle(true, 1.f);

	// Load another font, with larger size
	sf::Texture texture;

	ImGuiIO& io = ImGui::GetIO();
	unsigned char* pixels;
	int width, height;
	io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);
	ImFont* codeFont = io.Fonts->AddFontFromFileTTF("../ide/fonts/VeraMono.ttf", 18.f);
	io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);

	texture.create(width, height);
	texture.update(pixels);

	io.Fonts->TexID = (void*)texture.getNativeHandle();

	io.Fonts->ClearInputData();
	io.Fonts->ClearTexData();


	float mouseScroll = 0.f;

	g_Console.AddLog(Console::LogLevel::Info, "Welcome to the BrainJam IDE!\n");

	// For now, we can only have one file open at a time
	codeFile file;

	// Main window loop
	while(window.isOpen())
	{
		sf::Event event;
		while(window.pollEvent(event))
		{
			// Handle imgui events
			ImGui::SFML::ProcessEvent(event);

			// Handle window closed
			if(event.type == sf::Event::Closed)
			{
				// Use this to check if user wants to save or not
				if(file.New())
				{
					window.close();
				}
			}

			// Handle hotkeys
			if(event.type == sf::Event::KeyPressed)
			{
				if(event.key.code == sf::Keyboard::S && event.key.control && event.key.shift)
				{
					file.SaveAs();
				}
				else if(event.key.code == sf::Keyboard::S && event.key.control)
				{
					file.Save();
				}
				else if(event.key.code == sf::Keyboard::N && event.key.control)
				{
					file.New();
				}
				else if(event.key.code == sf::Keyboard::O && event.key.control)
				{
					file.Load();
				}
				else if(event.key.code == sf::Keyboard::R && event.key.control)
				{
					file.RecalculateColours();
				}

				else if(event.key.code == sf::Keyboard::F4 && event.key.control)
				{
					compile(file);
				}
				else if(event.key.code == sf::Keyboard::F5 && event.key.control)
				{
					compileAndRun(file);
				}


				// Debug
				else if(event.key.code == sf::Keyboard::D && event.key.control)
				{
					g_Console.AddLog(Console::LogLevel::Debug, "%zu\n", file.m_cursorPos);
				}
			}

			// Mouse scroll
			if(event.type == sf::Event::MouseWheelScrolled)
			{
				mouseScroll = event.mouseWheelScroll.delta;
			}
			else
			{
				mouseScroll = 0.f;
			}
		}

		// Update imgui
		{
			ImGui::SFML::Update(window, deltaClock.restart());
			// Always draw the titlebar/menubar
			DoTitleBar(file, g_Console.m_showConsole);

			g_Console.Draw("Log", &g_Console.m_showConsole);

			// Set up window attributes
			ImGui::SetNextWindowPos(ImVec2(0.f, 0.f));
			ImGui::SetNextWindowSize(ImVec2(window.getSize().x, window.getSize().y));
			ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);

			// Create window that covers everything
			ImGui::Begin("##main_window", nullptr,
			             ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoResize |
			             ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove |
			             ImGuiWindowFlags_NoScrollWithMouse);

			// Add spacing so title bar doesn't obstruct anything
			ImGui::Spacing();
			ImGui::Spacing();
			ImGui::Spacing();
			ImGui::Spacing();

			// Put everything else in the window
			DoHomepage(mouseScroll, file, codeFont);

			ImGui::End();

			// Pop FrameRounding
			ImGui::PopStyleVar();
		}

		// Display
		window.clear();
		ImGui::SFML::Render(window);
		window.display();
	}

	ImGui::SFML::Shutdown();
}

bool ide::compile(codeFile& file, std::string fileName)
{
	compiler::builder builder(file.m_code);
	if(!builder.buildExecutable(fileName))
	{
		std::cerr << "Error building executable!" << std::endl;
		g_Console.AddLog(Console::LogLevel::Error, "Error building executable!\n");
		return false;
	}
	return true;
}

bool ide::compileAndRun(codeFile& file)
{
	compiler::builder builder(file.m_code, 2048);
	std::string programName = "a.out";

	if(!compile(file, programName))
	{
		return false;
	}
	
	std::cout << "Compiled successfully!" << std::endl;
	g_Console.AddLog(Console::LogLevel::Info, "Compiled successfully!\n");
	std::cout << "---Program---" << std::endl;

	std::string command = "chmod +x ";

	command += programName;
	system(command.c_str());

	command = "./";
	command += programName;

	// NOTE: popen is NOT secure
	// TODO: For some reason, this refuses to read the output
	//	of the programs I run, but it does get the output for
	//	normal commands. ie.
	//	ls 			<-- works
	// 	./a.out 	<-- does not work
	std::string result = ide::exec(command.c_str());

	command = "echo $?";	// Get return code
	result = "Program exited with return code: ";
	result += ide::exec(command.c_str());
	g_Console.AddLog(Console::LogLevel::ProgramOutput, result.c_str());

	std::cout << std::endl << "---End of Program---" << std::endl;

	std::cout << "Result: " << result << std::endl;

	std::cin.sync();

	return true;
}
