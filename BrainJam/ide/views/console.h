#ifndef CONSOLE_H
#define CONSOLE_H

#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>

#include <imgui/imgui.h>

namespace ide
{
	// More like 'logger' than 'console' but whatever
	class Console
	{
		public:
			enum class LogLevel
			{
			    Invalid,

			    // General use
			    ProgramOutput,
			    Info,
			    Warning,
			    Error,

			    // Not general use
			    Debug,
			};

		private:

			ImGuiTextBuffer     Buf;
			ImGuiTextBuffer     m_colourized;
			ImGuiTextFilter     Filter;
			ImVector<int>       LineOffsets;        // Index to lines offset
			bool                ScrollToBottom;
		public:
			bool m_showConsole = true;


		public:
			void    Clear();

			void    AddLog(LogLevel level, const char* fmt, ...) IM_FMTARGS(3);

			void    Draw(const char* title, bool* p_open = NULL);
	};

	// Run a system command and get it's output back as a string
	std::string inline exec(std::string cmd)
	{
		std::string data;
		FILE * stream;
		const int max_buffer = 256;
		char buffer[max_buffer];
		cmd.append(" 2>&1");

		stream = popen(cmd.c_str(), "r");
		if(stream)
		{
			while(!feof(stream))
			{
				if(fgets(buffer, max_buffer, stream) != NULL)
				{
					data.append(buffer);
				}
			}
			pclose(stream);
		}
		return data;
	}
}

extern ide::Console g_Console;

#endif
