#include "console.h"

#include <time.h>
#include <imgui-util/imgui-util.h>

ide::Console g_Console;

void ide::Console::AddLog(LogLevel level, const char* fmt, ...)
{
	char buff[20];
	struct tm *sTm;
	int old_size = Buf.size();

	std::string format = "%s%s";

	std::string loglevel;
	switch(level)
	{
		case LogLevel::Info:
		{
			loglevel = "[INFO]:    ";
			break;
		}
		case LogLevel::Warning:
		{
			m_showConsole = true;
			loglevel = "[WARNING]: ";
			break;
		}
		case LogLevel::Error:
		{
			m_showConsole = true;
			loglevel = "[ERROR]:   ";
			break;
		}
		case LogLevel::ProgramOutput:
		{
			loglevel = "[Output]:  ";
			break;
		}
		case LogLevel::Debug:
		{
			loglevel = "[-DEBUG-]: ";
			break;
		}
	}

	time_t now = time(0);
	sTm = localtime(&now);

	strftime(buff, sizeof(buff), "[%H:%M:%S]", sTm);


	va_list args;
	va_start(args, fmt);
	Buf.append(format.c_str() , buff, loglevel.c_str());
	Buf.appendv(fmt, args);
	va_end(args);
	for(int new_size = Buf.size(); old_size < new_size; old_size++)
	{
		if (level == LogLevel::Error)
		{
			m_colourized.append("%c", static_cast<char>(imgui_util::SyntaxColours::Error));
		}
		else if (level == LogLevel::Warning)
		{
			m_colourized.append("%c", static_cast<char>(imgui_util::SyntaxColours::Warning));
		}
		else if (level == LogLevel::Info)
		{
			m_colourized.append("%c", static_cast<char>(imgui_util::SyntaxColours::Info));
		}
		else if (level == LogLevel::ProgramOutput)
		{
			m_colourized.append("%c", static_cast<char>(imgui_util::SyntaxColours::Success));
		}
		else if (level == LogLevel::Debug)
		{
			m_colourized.append("%c", static_cast<char>(imgui_util::SyntaxColours::Debug));
		}
		else
		{
			m_colourized.append("%c", static_cast<char>(imgui_util::SyntaxColours::Regular));
		}
		
		if(Buf[old_size] == '\n')
		{
			LineOffsets.push_back(old_size);
		}
	}
	ScrollToBottom = true;
}

void ide::Console::Draw(const char* title, bool* p_open)
{
	if(!p_open || !*(p_open))
	{
		return;
	}
	ImGui::SetNextWindowSize(ImVec2(500, 400), ImGuiCond_FirstUseEver);
	ImGui::Begin(title, p_open, ImGuiWindowFlags_NoCollapse);
	if(ImGui::Button("Clear"))
	{
		Clear();
	}
	ImGui::SameLine();
	ImGui::SameLine();
	Filter.Draw("Filter", -50.0f);
	ImGui::Separator();
	ImGui::BeginChild("scrolling", ImVec2(0, 0), false, ImGuiWindowFlags_HorizontalScrollbar);

	if(Filter.IsActive())
	{
		const char* buf_begin = Buf.begin();
		const char* line = buf_begin;
		const char* colourized_line = m_colourized.c_str();
		for(int line_no = 0; line != NULL; line_no++)
		{
			const char* line_end = (line_no < LineOffsets.Size) ? buf_begin + LineOffsets[line_no] : NULL;
			const char* colourized_line_end = (line_no < LineOffsets.Size) ? colourized_line + LineOffsets[line_no] : NULL;
			if(Filter.PassFilter(line, line_end))
			{
				ImGui::TextUnformatted(line, line_end, colourized_line);
			}
			line = line_end && line_end[1] ? line_end + 1 : NULL;
			colourized_line = colourized_line_end && colourized_line_end[1] ? colourized_line_end + 1 : NULL;
		}
	}
	else
	{
		ImGui::TextUnformatted(Buf.begin(), NULL, m_colourized.c_str());
	}

	if(ScrollToBottom)
	{
		ImGui::SetScrollHere(1.0f);

	}
	ScrollToBottom = false;
	ImGui::EndChild();
	ImGui::End();
}

void ide::Console::Clear()
{
	Buf.clear();
	LineOffsets.clear();
}
