#ifndef HOMEPAGE_H
#define HOMEPAGE_H

#include <iostream>

#include <codeFile.h>
#include <imgui-util/imgui-util.h>
#include <limits>


namespace ide
{
	void DoHomepage(const float scrollAmount, codeFile& file, ImFont* codeFont)
	{
		static bool read_only = false;

		// Display filename
		ImGui::BeginChild("##filename", ImVec2(0.f, 0.f), false, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);
		{
			ImGui::TextColored(file.IsSaved() ? ImVec4(0.39f, 0.98f, 0.36f, 1.00f) : ImVec4(0.98f, 0.39f, 0.39f, 1.00f), "File: ");
			ImGui::SameLine();
			ImGui::TextColored(ImVec4(0.39f, 0.39f, 0.39f, 1.00f), "%s", file.GetFileName().c_str());

			// We need this monstrosity because the InputText keeps track of the buffer
			// So we need it to lose focus for a frame in order to not store the copy
			if(file.m_wasReset)
			{
				file.m_wasReset = false;
			}
			else
			{
				// Actual window
				ImGui::BeginChild("##homepage", ImVec2(0.f, 0.f), false, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);
				{
					ImGui::PushFont(codeFont);

					float boxSize =
					    floor((ImGui::GetWindowSize().y)								// Window size
					          / ImGui::GetTextLineHeight())								// Get number of lines
					    * ImGui::GetTextLineHeight();									// Get total size (numlines * size of line)

					if(ImGui::InputTextMultiline(
					            "##source",
					            file.m_code,
					            file.m_codeSize,
					            ImVec2(-1.0f, boxSize),
					            ImGuiInputTextFlags_AllowTabInput | ImGuiInputTextFlags_CallbackAlways | (read_only ? ImGuiInputTextFlags_ReadOnly : 0),
					            file.m_colourized,
					            [](ImGuiTextEditCallbackData * data)
				{
					ide::codeFile* extFile = static_cast<ide::codeFile*>(data->UserData);

						extFile->m_cursorPos = data->CursorPos;
						extFile->RecalculateColours();

						return 0;
					}, static_cast<void*>(&file)))
					{
						file.Update();
					}

					ImGui::PopFont();

					// Reset cursor_pos if window loses focus
					if(!ImGui::IsWindowFocused())
					{
						file.m_cursorPos = static_cast<size_t>(std::numeric_limits<size_t>::max());	// max it out
						file.RecalculateColours();
					}

					ImGui::BeginChild("##source");
					{
						ImGui::SetScrollY(ImGui::GetScrollY() - scrollAmount);
					}
					ImGui::EndChild();


				}
				ImGui::EndChild();
			}
		}
		ImGui::EndChild();

	}
}

#endif
