#ifndef TITLEBAR_H
#define TITLEBAR_H

#include <imgui/imgui.h>
#include <imgui-sfml/imgui-SFML.h>

#include <app.h>
#include <codeFile.h>
#include <compiler/builder.h>

#include <views/console.h>

namespace ide
{
	inline void DoTitleBar(codeFile& file, bool& showConsole)
	{
		if(ImGui::BeginMainMenuBar())
		{
			if(ImGui::BeginMenu("File"))
			{
				if(ImGui::MenuItem("New", "CTRL+N"))
				{
					file.New();
				}
				if(ImGui::MenuItem("Open", "CTRL+O"))
				{
					file.Load();
				}
				if(ImGui::MenuItem("Save", "CTRL+S"))
				{
					file.Save();
				}
				if(ImGui::MenuItem("Save As", "CTRL+SHIFT+S"))
				{
					file.SaveAs();
				}
				ImGui::EndMenu();
			}
			if(ImGui::BeginMenu("Edit"))
			{
				// TODO: Implement Copy/Paste
				// 	Looks like there's no standard way to do this on Linux
				//	It seems everything goes through a program called 'X' or 'xsel'
				//	but copy/paste happens through requests to that program, which then
				//	passes on the request to the original program (the one that has the string
				//	we want to copy). Long story short, seems like a non-trivial task to implement.
				if(ImGui::MenuItem("Recalculate Colours", "CTRL+R"))
				{
					file.RecalculateColours();
				}
				ImGui::EndMenu();
			}
			if(ImGui::BeginMenu("Build"))
			{
				if(ImGui::MenuItem("Compile", "CTRL+F4"))
				{
					compile(file);
				}
				if(ImGui::MenuItem("Compile and Run", "CTRL+F5"))
				{
					compileAndRun(file);
				}
				ImGui::EndMenu();
			}
			if(ImGui::BeginMenu("Help"))
			{
				if(ImGui::MenuItem("Show Logs", ""))
				{
					showConsole = true;
				}
				ImGui::Separator();
				if(ImGui::MenuItem("About", ""))
				{
					//TODO: Create an About popup
				}
				ImGui::EndMenu();
			}
			ImGui::EndMainMenuBar();
		}
	}
}

#endif
